﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using Photon.Pun;
public class Bullet : MonoBehaviourPun
{
    public float damage;
    public string source;
    public int id;
    private void Start()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.CompareTag("Player") && other.transform.root.gameObject.GetComponent<PhotonView>().Owner.NickName != source)  
        {

            
            Health playerHealth = other.gameObject.transform.root.gameObject.GetComponent<Health>();
            playerHealth.TakeDamage(damage,source,id);
            Destroy(gameObject);
        }
    }

   
}
