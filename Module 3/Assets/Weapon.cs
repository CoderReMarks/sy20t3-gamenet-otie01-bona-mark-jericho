﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class Weapon : MonoBehaviourPun
{
    public Transform firePoint;
    public float fireRate;
    public int damage;
    public bool canShoot;
    public bool isShooting;
    public Coroutine currentCoroutine;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space) && gameObject.GetComponent<PhotonView>().IsMine)
        {

            Shoot();
        }
    }

    [PunRPC]
    public virtual void RPCShoot()
    {
 
    }
    public virtual void Shoot()
    {
        gameObject.GetComponent<PhotonView>().RPC("RPCShoot", RpcTarget.All);

    }

    protected IEnumerator Cooldown()
    {

        yield return new WaitForSeconds(fireRate);
        isShooting = false;
        canShoot = true;
    }
}
