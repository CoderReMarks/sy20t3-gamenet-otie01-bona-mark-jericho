﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;
public class KillController : MonoBehaviourPunCallbacks
{
    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }
    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventsCode.PlayerElimination)
        {
            GetComponent<PlayerSetUp>().camera.transform.parent = null;
            GetComponent<VehicleMovement>().enabled = false;
            GetComponent<Weapon>().enabled = false;
            object[] data = (object[])photonEvent.CustomData;
            string victim = (string)data[0];
            string killer = (string)data[1];
            int viewID = (int)data[2];
            int killerViewID = (int)data[3];
            GameObject announcerUIText = DeathRaceGameManager.instance.deathAnnouncer;
            announcerUIText.SetActive(true);

            announcerUIText.GetComponent<Text>().text = killer + " Killed " + victim;
            announcerUIText.GetComponent<Text>().color = Color.red;

            ////victim Placement leaderboard
            //object[] dataForVictim = new object[] { victim, viewID };


            //RaiseEventOptions raiseEventsOptionsForVictim = new RaiseEventOptions
            //{
            //    Receivers = ReceiverGroup.All,
            //    CachingOption = EventCaching.AddToRoomCache
            //};

            //SendOptions sendOptionForVictim = new SendOptions { Reliability = false };

            //PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoFinishedEventCode, dataForVictim, raiseEventsOptionsForVictim, sendOptionForVictim);

            if (DeathRaceGameManager.instance.amountOfPlayersLeft == 1)
            {

                DeathRaceGameManager.instance.amountOfPlayersLeft--;

                //killer Placement leaderboard
                object[] dataForLastStandingMan = new object[] { killer, killerViewID };

                
                RaiseEventOptions raiseEventsOptionsForLastStandingMan = new RaiseEventOptions
                {
                    Receivers = ReceiverGroup.All,
                    CachingOption = EventCaching.AddToRoomCache
                };

                SendOptions sendOptionForLastStandingMan = new SendOptions { Reliability = false };

                PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoFinishedEventCode, dataForLastStandingMan, raiseEventsOptionsForLastStandingMan, sendOptionForLastStandingMan);

                //Winner text
                object[] dataForWinner = new object[] { killer };


                RaiseEventOptions raiseEventsOptionsForWinner = new RaiseEventOptions
                {
                    Receivers = ReceiverGroup.All,
                    CachingOption = EventCaching.AddToRoomCache
                };

                SendOptions sendOptionForWinner = new SendOptions { Reliability = false };

                PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.PlayerWon, dataForWinner, raiseEventsOptionsForWinner, sendOptionForWinner);

            }
        }
        else if (photonEvent.Code == (byte)RaiseEventsCode.WhoFinishedEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;
            string nickNameOfFinishedPlayer = (string)data[0];

            int viewID = (int)data[1];


            GameObject orderUIText = DeathRaceGameManager.instance.finisherTextUI[DeathRaceGameManager.instance.amountOfPlayersLeft];
            foreach (GameObject placement in DeathRaceGameManager.instance.finisherTextUI)
            {
                placement.SetActive(true);
            }
           
            if (viewID == photonView.ViewID)
            {
                orderUIText.GetComponent<Text>().text = (DeathRaceGameManager.instance.amountOfPlayersLeft + 1) + " " + nickNameOfFinishedPlayer + " (YOU) ";
                orderUIText.GetComponent<Text>().color = Color.red;
            }
            else
            {
                orderUIText.GetComponent<Text>().text = (DeathRaceGameManager.instance.amountOfPlayersLeft + 1)+ " " + nickNameOfFinishedPlayer;
            }   
        }
        else if (photonEvent.Code == (byte)RaiseEventsCode.PlayerWon)
        {
            object[] data = (object[])photonEvent.CustomData;
            string nickNameOfFinishedPlayer = (string)data[0];



            Text winnerText = DeathRaceGameManager.instance.winnerText;
            winnerText.gameObject.SetActive(true);

            winnerText.text = nickNameOfFinishedPlayer + " WON ";
            winnerText.color = Color.red;
 
        }
    }
}
