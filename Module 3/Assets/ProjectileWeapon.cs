﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class ProjectileWeapon : Weapon
{
    public GameObject prefab;
    

    public float projectileSpeed;
  
    [PunRPC]
    public override void RPCShoot()
    {
        if (canShoot == true)
        {
            if (isShooting == false)
            {
                isShooting = true;
                canShoot = false;
                //GameObject newProjectile = (GameObject)PhotonNetwork.Instantiate("Projectile", firePoint.position, Quaternion.Euler(firePoint.forward));
                GameObject newProjectile = Instantiate(prefab, firePoint.position, Quaternion.Euler(firePoint.forward));
                newProjectile.transform.forward = firePoint.forward;
                newProjectile.GetComponent<ConstantForce>().force = firePoint.forward.normalized * (projectileSpeed + gameObject.GetComponent<VehicleMovement>().speed);
                newProjectile.GetComponent<Bullet>().source = photonView.Owner.NickName;
                newProjectile.GetComponent<Bullet>().id = photonView.ViewID;
                currentCoroutine = StartCoroutine(Cooldown());
                Destroy(newProjectile, 5);
            }
        }
    }


}
