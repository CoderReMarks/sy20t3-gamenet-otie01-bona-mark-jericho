﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{
    public float speed;
    public float maxSpeed;

    public float rotationSpeed;

    public bool isControlEnabled;
    public Rigidbody rb;
    private void Start()
    {
        isControlEnabled = false;
    }
    private void FixedUpdate()
    {
        if (isControlEnabled)
        {
            float translation = Input.GetAxis("Vertical");
            float rotation = Input.GetAxis("Horizontal");

            rb.AddRelativeForce(transform.worldToLocalMatrix.MultiplyVector(gameObject.transform.forward)*translation * speed, ForceMode.Acceleration);

            if (rotation != 0)
            {
                rb.AddRelativeForce(-transform.worldToLocalMatrix.MultiplyVector(gameObject.transform.forward) * translation * 1, ForceMode.Acceleration);
                rb.AddRelativeTorque(Vector3.up * rotation * rotationSpeed, ForceMode.VelocityChange);
            }
            
            if (rb.velocity.magnitude > maxSpeed)
            {
               
                rb.velocity = rb.velocity.normalized * maxSpeed;
            }
        }
 
        
    }
}
