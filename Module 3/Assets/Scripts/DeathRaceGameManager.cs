﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class DeathRaceGameManager : MonoBehaviour
{
    public GameObject[] vehiclePrefab;
    public Transform[] startingPositions;
    public GameObject deathAnnouncer;
    public GameObject[] finisherTextUI;
    public static DeathRaceGameManager instance = null;
    public int amountOfPlayersLeft;
    public Text timeText;
    public Text winnerText;

 
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerSelectionNumber;
            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELETION_NUMBER, out playerSelectionNumber))
            {
                //Debug.Log((int)playerSelectionNumber);
                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = startingPositions[actorNumber - 1].position;
                PhotonNetwork.Instantiate(vehiclePrefab[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity);
            }
            foreach (GameObject go in finisherTextUI)
            {
                go.SetActive(false);
            }
        }

  
    }
}
