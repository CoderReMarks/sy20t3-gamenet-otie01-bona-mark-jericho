﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;
public class PlayerSetUp : MonoBehaviourPunCallbacks
{
    public Camera camera;
    // Start is called before the first frame update
    void Start()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            GetComponent<Weapon>().enabled = false;
            GetComponent<KillController>().enabled = false;
            camera.enabled = photonView.IsMine;
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = false;
            GetComponent<Weapon>().enabled = false;
            GetComponent<KillController>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
            DeathRaceGameManager.instance.amountOfPlayersLeft++;
        }
    }

  

    // Update is called once per frame
    void Update()
    {
        
    }
}
