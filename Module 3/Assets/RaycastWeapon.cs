﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class RaycastWeapon : Weapon
{

    public Camera cam;


    [PunRPC]
    public override void RPCShoot()
    {
        if (canShoot == true)
        {
            if (isShooting == false)
            {
                isShooting = true;
                canShoot = false;
                RaycastHit hit;
                Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f));
                //if (Physics.Raycast(ray, out hit, 200))
                if (Physics.Raycast(firePoint.position, firePoint.forward,out hit,200))
                {
                    Debug.Log("SHOT");
                    if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.transform.root.gameObject.GetComponent<PhotonView>().IsMine)
                    {
                        Debug.Log("HIT");
                        Health playerHealth = hit.collider.gameObject.transform.root.gameObject.GetComponent<Health>();
                        playerHealth.TakeDamage(damage, photonView.Owner.NickName,photonView.ViewID);
                    }
                }
                currentCoroutine = StartCoroutine(Cooldown());
            }
        }
    }
    

  
}
