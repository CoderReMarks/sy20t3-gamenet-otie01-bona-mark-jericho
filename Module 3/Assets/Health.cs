﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon;
using ExitGames.Client.Photon;
using Photon.Realtime;
using UnityEngine.UI;
public class Health : MonoBehaviourPun
{
    public bool isAlive;
    public float health;
    public float maxHealth;
    public Image healthBar;
    // Start is called before the first frame update
    void Start()
    {
        isAlive = true;
        health = maxHealth;
        healthBar.fillAmount = health / maxHealth;
    }

    public void TakeDamage(float p_amount,string p_source, int p_killerViewID)
    {
        Debug.Log(p_amount + " - " + p_source + " - " + p_killerViewID);
        Debug.Log(photonView.IsMine);
        if (!photonView.IsMine)
        {
            
            photonView.RPC("Damage", RpcTarget.AllBuffered, p_amount,p_source,photonView.ViewID, p_killerViewID);
        }
        
    
    }

    [PunRPC]
    void Damage(float p_amount,string p_source, int p_viewID, int p_killerViewID)
    {
        Debug.Log(isAlive);
        if (isAlive)
        {
            Debug.Log("HURT");
            health -= p_amount;
            healthBar.fillAmount = health / maxHealth;
            if (health <= 0)
            {
                Death(p_source, p_viewID, p_killerViewID);
            }
        }
   

    }

    void Death(string p_source, int p_viewID, int p_killerViewID)
    {
        isAlive = false;
        DeathRaceGameManager.instance.amountOfPlayersLeft--;
        //event data for placement

        string nickName = photonView.Owner.NickName;
        int viewID = photonView.ViewID;

        //event data
        object[] dataPlacement = new object[] { nickName, viewID };


        RaiseEventOptions raiseEventsOptionsPlacement = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptionPlacement = new SendOptions { Reliability = false };

        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoFinishedEventCode, dataPlacement, raiseEventsOptionsPlacement, sendOptionPlacement);


        //event data for death announcer
        object[] data = new object[] { photonView.Owner.NickName, p_source, p_viewID,  p_killerViewID };


        RaiseEventOptions raiseEventsOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions { Reliability = false };

        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.PlayerElimination, data, raiseEventsOptions, sendOption);



    }
}
